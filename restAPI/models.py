from django.contrib.gis.db import models
from users.models import User

class Driver(User):
    # Regular Django fields corresponding to the attributes in the
    # world borders shapefile.
	BUSY='BS'
	AVAILABLE='AV'
	ON_BREAK='OB'
	OFF_DUTY='OD'
	STATUS_CHOICES=(
		(BUSY,"Busy"),
		(AVAILABLE,"Available"),
		(ON_BREAK,"On Break"),
		(OFF_DUTY,"Off Duty")
	)
    location = models.PointField()
    serviceArea = models.MultiPolygonField()
	status = models.CharField(
		max_length=2,
		choices=STATUS_CHOICES,
		default=OFF_DUTY,
	)
	
class Job(models.Model):
	requestTime = models.DateTimeField()
	requestedPickupTime = models.DateTimeField()
	actualPickupTime = models.DateTimeField()
	dropoffTime = models.DateTimeField()
	startAddress = models.CharField(max_length=400,null=True,blank=True)
	startLocation = models.PointField(null=True,blank=True)
	endLocation = models.CharField(max_length=400,null=True,blank=True)
	endLocation = models.PointField(null=True,blank=True)
	driver = models.ForeignKey("Driver",null=True,blank=True)
	